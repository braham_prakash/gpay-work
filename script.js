window.onload = () => {
  const toggleBtn = document.querySelector(".toggle_icons_btn");
  const controlsDot = document.querySelectorAll(".controls__dot");
  const carousel = document.querySelector(
    ".carousel_main_container_mob .carousel"
  );
  const carouselItems = carousel.querySelectorAll(".carousel__item");
  const citiesDropDown = document.querySelector(".citiesDropDown");
  const diseaseDropDown = document.querySelector(".diseaseDropDown");
  const cityInpBox = document.querySelector("#cityInp");
  const diseaseInpBox = document.querySelector("#diseaseInp");
  const cityCrossBtnContainer = document.querySelector(
    ".city_crossBtnContainer"
  );
  const diseaseCrossBtnContainer = document.querySelector(
    ".disease_crossBtnContainer"
  );

  const finalCitiesAndDisease = {};

  let cityCross = false;
  let diseaseCross = false;
  // const controls = document.querySelector(".controls")s
  // console.log(carousel);
  // console.log(citiesDropDown);

  (async function getCitiesAndDiseases() {
    let citiesArr = [
      "Delhi",
      "Gurgaon",
      "Faridabad",
      "Ghaziabad",
      "Noida",
      "Hyderabad",
      "Bangalore",
      "Chennai",
      "Kolkata",
      "Mumbai",
      "Pune",
      "Jaipur",
      "Nagpur",
      "Lucknow",
      "Ludhiana",
      "Kanpur",
      "Gwalior",
      "Chandigarh",
      "Agra",
      "Bhopal",
      "Patna",
      "Bhubaneswar",
      "Indore",
      "Siliguri",
      "Kozhikode",
      "Meerut",
    ];

    let diseaseArr = [
      "Anal Fissure",
      "Anal Fistula",
      "Piles",
      "Pilonidal Sinus",
      "Appendicitis",
      "Gallstones",
      "Hernia",
      "Rectal Prolapse",
      "Umbilical Hernia",
      "Female Urinary Problems",
      "Hymenoplasty",
      "Hysterectomy",
      "Hysteroscopy",
      "Irregular Periods",
      "Labiaplasty",
      "Laser Vaginal Rejuvenation",
      "Laser Vaginal Tightening",
      "Lichen Sclerosus",
      "Medical Termination Of Pregnancy",
      "Menstrual Disorders",
      "Ovarian Cyst",
      "Pain During Intercourse",
      "Pap Smear",
      "PCOS PCOD",
      "Pelvic Pain",
      "Preconception Care",
      "Pregnancy Care",
      "Stress Urinary Incontinence",
      "Urinary Tract Infection",
      "Uterine Fibroid",
      "Vaginal Cyst",
      "Vaginal Discharge",
      "Vaginal Dryness",
      "Vaginal Recurrent Infection",
      "Vaginoplasty",
      "Adenoidectomy",
      "Deviated Nasal Septum",
      "Mastoidectomy",
      "ENT-Others",
      "Microlaryngeal",
      "Myringotomy",
      "Ruptured Eardrum",
      "Septoplasty",
      "Sinusitis",
      "Tongue base reduction",
      "Tonsillitis",
      "Turbinate reduction",
      "Uvulopalatopharyngoplasty",
      "Circumcision",
      "Cystoscopy",
      "Erectile Dysfunction",
      "Hydrocele",
      "Kidney Stone",
      "Male Urinary Tract Infection",
      "Phimosis",
      "Prostate Enlargement",
      "Urethral Stricture",
      "Urinary Incontinence",
      "Deep Vein Thrombosis",
      "Diabetic Foot Ulcer",
      "Spider Veins",
      "Varicocele",
      "Varicose Veins",
      "Breast Lift",
      "Gynecomastia",
      "Hair Transplant",
      "Lipoma",
      "Liposuction",
      "Rhinoplasty",
      "Sebaceous Cyst",
      "ACL Tear",
      "Arthroscopy",
      "Carpal Tunnel Syndrome",
      "Hip Replacement",
      "Knee Replacement",
      "Cataract Surgery",
      "Lasik Surgery",
      "Female Infertility",
      "IUI",
      "IVF",
      "Male Infertility",
    ];

    citiesArr.sort();

    finalCitiesAndDisease["citiesArr"] = citiesArr;
    finalCitiesAndDisease["diseaseArr"] = diseaseArr;

    populateCitiesAndDisease(citiesArr, diseaseArr);
  })();

  function populateCitiesAndDisease(cityArr, diseaseArr) {
    cityArr.forEach(function (ele) {
      let cityDiv = document.createElement("div");
      cityDiv.classList.add("cityDiv");
      cityDiv.textContent = ele;
      cityDiv.addEventListener("click", selectedCityInp);
      citiesDropDown.appendChild(cityDiv);
    });

    diseaseArr.forEach(function (ele) {
      let diseaseDiv = document.createElement("div");
      diseaseDiv.classList.add("diseaseDiv");
      diseaseDiv.textContent = ele;
      diseaseDiv.addEventListener("click", selectedDiseaseInp);
      diseaseDropDown.appendChild(diseaseDiv);
    });
  }

  function selectedCityInp(e) {
    let prevDelCityBtn = document.querySelector(".delCityBtn");
    if (prevDelCityBtn) {
      prevDelCityBtn.remove();
    }
    cityCross = true;
    let selectedCity = e.target.textContent;
    cityInpBox.value = selectedCity;
    let delCityBtn = document.createElement("span");
    delCityBtn.classList.add("delCityBtn");
    delCityBtn.textContent = "✖";
    delCityBtn.style.color = "#5f5f5f";
    delCityBtn.addEventListener("click", removeCityFromInp);
    cityCrossBtnContainer.append(delCityBtn);
  }

  function selectedDiseaseInp(e) {
    let prevDelDiseaseBtn = document.querySelector(".delDiseaseBtn");
    if (prevDelDiseaseBtn) {
      prevDelDiseaseBtn.remove();
    }
    diseaseCross = true;
    let selectedDisease = e.target.textContent;
    diseaseInpBox.value = selectedDisease;
    let delDiseaseBtn = document.createElement("span");
    delDiseaseBtn.classList.add("delDiseaseBtn");
    delDiseaseBtn.textContent = "✖";
    delDiseaseBtn.style.color = "#5f5f5f";
    delDiseaseBtn.addEventListener("click", removeDiseaseFromInp);
    diseaseCrossBtnContainer.append(delDiseaseBtn);
  }

  function removeCityFromInp(e) {
    cityCross = false;
    cityInpBox.value = "";
    let allCities = document.querySelectorAll(".cityDiv");
    for (let i = 0; i < allCities.length; i++) {
      allCities[i].style.display = "";
    }
    e.target.remove();
  }

  function removeDiseaseFromInp(e) {
    diseaseCross = false;
    diseaseInpBox.value = "";
    let allDisease = document.querySelectorAll(".diseaseDiv");
    for (let i = 0; i < allDisease.length; i++) {
      allDisease[i].style.display = "";
    }
    e.target.remove();
  }

  function cityFilterFunction(e) {
    if (cityCross) {
      let delCityBtn = cityCrossBtnContainer.querySelector(".delCityBtn");
      delCityBtn.remove();
      cityCross = false;
    }

    let city = e.target.value;
    let cityInp = city.trim().toUpperCase();
    let allCities = citiesDropDown.querySelectorAll(".cityDiv");

    let isCityExist = false;

    for (i = 0; i < allCities.length; i++) {
      let txtValue = allCities[i].textContent || allCities[i].innerText;
      if (txtValue.toUpperCase().indexOf(cityInp) > -1) {
        allCities[i].style.display = "";
      } else {
        allCities[i].style.display = "none";
      }

      if (allCities[i].style.display !== "none") {
        isCityExist = true;
      }
    }

    if (!isCityExist) {
      let prevEle = citiesDropDown.querySelector(".no_city");
      if (prevEle) {
        return;
      }

      let noCityFound = document.createElement("div");
      noCityFound.textContent = "No City Found";
      noCityFound.classList.add("no_city");
      citiesDropDown.appendChild(noCityFound);
    } else {
      let noCityFound = citiesDropDown.querySelector(".no_city");
      if (noCityFound) noCityFound.remove();
    }
  }

  function diseaseFilterFunction(e) {
    if (diseaseCross) {
      let delDiseaseBtn =
        diseaseCrossBtnContainer.querySelector(".delDiseaseBtn");
      delDiseaseBtn.remove();
      diseaseCross = false;
    }

    let disease = e.target.value;
    // lookupDisease = disease;
    // console.log(lookupDisease);
    let diseaseInp = disease.trim().toUpperCase();
    let allDisease = diseaseDropDown.querySelectorAll(".diseaseDiv");

    let isDiseaseExist = false;

    for (i = 0; i < allDisease.length; i++) {
      let txtValue = allDisease[i].textContent || allDisease[i].innerText;
      if (txtValue.toUpperCase().indexOf(diseaseInp) > -1) {
        allDisease[i].style.display = "";
      } else {
        allDisease[i].style.display = "none";
      }

      if (allDisease[i].style.display !== "none") {
        isDiseaseExist = true;
      }
    }

    if (!isDiseaseExist) {
      let prevEle = diseaseDropDown.querySelector(".no_disease");
      if (prevEle) {
        return;
      }

      let noDiseaseFound = document.createElement("div");
      noDiseaseFound.textContent = "No Disease Found";
      noDiseaseFound.classList.add("no_disease");
      diseaseDropDown.appendChild(noDiseaseFound);
    } else {
      let noDiseaseFound = diseaseDropDown.querySelector(".no_disease");
      if (noDiseaseFound) noDiseaseFound.remove();
    }
  }

  function toggleCityDropDown(e) {
    e.stopPropagation();
    if (!citiesDropDown.classList.contains("openCitiesDropDown"))
      citiesDropDown.classList.add("openCitiesDropDown");
    diseaseDropDown.classList.remove("openDiseaseDropDown");
  }

  function toggleDiseaseDropDown(e) {
    e.stopImmediatePropagation();
    if (!diseaseDropDown.classList.contains("openDiseaseDropDown"))
      diseaseDropDown.classList.add("openDiseaseDropDown");
    citiesDropDown.classList.remove("openCitiesDropDown");
  }

  cityInpBox.addEventListener("click", (e) => {
    e.stopPropagation();
  });
  diseaseInpBox.addEventListener("click", (e) => {
    e.stopPropagation();
  });

  window.onclick = function removeFocusFromInps() {
    diseaseDropDown.classList.remove("openDiseaseDropDown");
    citiesDropDown.classList.remove("openCitiesDropDown");
  };

  function handleToggleBtn() {
    let moreIcons = document.querySelector(".more_icons");
    moreIcons.classList.toggle("hide_icons");
    if (moreIcons.classList.contains("hide_icons")) {
      toggleBtn.textContent = "View More \u2304";
    } else {
      toggleBtn.textContent = "View Less \u2303";
    }
  }

  function handleControlDot(ev) {
    controlsDot.forEach((ele) => {
      ele.classList.remove("active_dot");
    });
    ev.target.classList.toggle("active_dot");
  }

  function isInViewport(el) {
    const rect = el.getBoundingClientRect();
    console.log(el);
    return (
      rect.left >= 0 &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  function handleCarouselScroll() {
    let selectedSlide;
    carouselItems.forEach((elm) => {
      let isVisible = isInViewport(elm);
      console.log(isVisible);
      if (isVisible) {
        selectedSlide = elm;
      }
    });

    if (selectedSlide) {
      let activeDot = document.querySelector(`a[href="#${selectedSlide.id}"]`);
      controlsDot.forEach((ele) => {
        ele.classList.remove("active_dot");
      });
      activeDot.classList.toggle("active_dot");
    }
  }

  cityInpBox.addEventListener("input", cityFilterFunction);
  diseaseInpBox.addEventListener("input", diseaseFilterFunction);

  cityInpBox.addEventListener("focus", toggleCityDropDown);
  diseaseInpBox.addEventListener("focus", toggleDiseaseDropDown);

  toggleBtn.addEventListener("click", handleToggleBtn);

  controlsDot.forEach((ele) => {
    ele.addEventListener("click", (e) => {
      e.stopImmediatePropagation();
      handleControlDot(e);
    });
  });

  carousel.addEventListener("scroll", handleCarouselScroll);

  const redirectPage = (delay) => {
    setTimeout(() => {
      window.location.href = "http://www.pristyncare.com/";
    }, delay);
  };

  const appointmentForm = document.querySelector(".appointment_form");

  const handleErrorMsg = ({ id, errorMsg }) => {
    const element = document.querySelector(`.errorMsgItem.${id}`);

    element.textContent = errorMsg;
  };

  const validateInput = (input) => {
    const { value, id } = input;

    switch (id) {
      case "couponInp": {
        if (value !== "EIFE87Y4VTMIQ3VT723A") {
          handleErrorMsg({
            id,
            errorMsg: "Please enter a valid coupon code",
          });

          return true;
        }

        handleErrorMsg({
          id,
          errorMsg: "",
        });

        break;
      }

      case "nameInp": {
        if (!!!value) {
          handleErrorMsg({
            id,
            errorMsg: "Name cant be blank",
          });

          return true;
        }

        if (value.match(/[^a-z _]/gim)) {
          handleErrorMsg({
            id,
            errorMsg: "Please enter a valid name",
          });

          return true;
        }

        document.querySelector(
          "#appointment-succes-popup .person-name"
        ).textContent = value;

        handleErrorMsg({
          id,
          errorMsg: "",
        });

        break;
      }

      case "mobileNoInp": {
        if (!!!value) {
          handleErrorMsg({
            id,
            errorMsg: "Phone cant be blank",
          });

          return true;
        }

        if (value.length !== 10) {
          handleErrorMsg({
            id,
            errorMsg: "Phone should be 10 digits",
          });

          return true;
        }

        if (value.match(/\D/gim)) {
          handleErrorMsg({
            id,
            errorMsg: "Only digits are valid",
          });

          return true;
        }

        handleErrorMsg({
          id,
          errorMsg: "",
        });
        break;
      }

      case "cityInp": {
        if (!!!value) {
          handleErrorMsg({
            id,
            errorMsg: "City cant be blank",
          });

          return true;
        }

        if (!!!finalCitiesAndDisease["citiesArr"].includes(value)) {
          handleErrorMsg({
            id,
            errorMsg: "City does not match with the list",
          });

          return true;
        }

        handleErrorMsg({
          id,
          errorMsg: "",
        });
        break;
      }

      case "diseaseInp": {
        if (!!!value) {
          handleErrorMsg({
            id,
            errorMsg: "Disease cant be blank",
          });

          return true;
        }

        if (!!!finalCitiesAndDisease["diseaseArr"].includes(value)) {
          handleErrorMsg({
            id,
            errorMsg: "Disease does not match with the list",
          });

          return true;
        }

        handleErrorMsg({
          id,
          errorMsg: "",
        });
        break;
      }

      default:
        break;
    }

    return false;
  };

  const validateAllElements = () => {
    const allInputs = document.querySelectorAll(
      ".appointment_form .input_fields input"
    );

    let someErrorFound = false;

    allInputs.forEach((input) => {
      if (someErrorFound) return;

      // if (input.id === "couponInp") return

      if (!!validateInput(input)) {
        someErrorFound = true;
      }
    });

    return someErrorFound;
  };

  document.querySelector(".appointment_form #apply_couponBtn").onclick = () => {
    const hasErrorFound = validateInput(
      document.querySelector(".appointment_form #couponInp")
    );

    if (hasErrorFound) {
      // console.log("if");
      return;
    }

    document.querySelector("#coupon-succes-popup").style.visibility =
      "visible";
  };

  appointmentForm.onsubmit = (e) => {
    e.preventDefault();

    const hasErrorFound = validateAllElements();

    if (hasErrorFound) {
      // console.log("if");
      return;
    }
    // console.log("else");

    document.querySelector("#appointment-succes-popup").style.visibility =
      "visible";
  };

  //pop ups js code -------------

  const hidePopup = (selector = null, event) => {
    if (selector) {
      document.querySelector(selector).style.visibility = "hidden";
    } else {
      event.target.parentElement.style.visibility = "hidden";
    }
  };

  document
    .querySelectorAll("[data-popup-element] .popup-bg")
    .forEach((element) => {
      element.onclick = (e) => {
        hidePopup(null, e);
      };
    });

  document.querySelector(
    "#appointment-succes-popup .popup-body .close-btn"
  ).onclick = (e) => {
    hidePopup("#appointment-succes-popup");
    redirectPage(0);
  };

  document.querySelector(
    "#coupon-succes-popup .popup-body .close-btn"
  ).onclick = (e) => {
    hidePopup("#coupon-succes-popup");
  };
};
